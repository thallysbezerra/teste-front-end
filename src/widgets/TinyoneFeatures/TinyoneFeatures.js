import React from 'react';
import OpeningText from '../../components/OpeningText/OpeningText';
import data from './TinyoneFeatures.json';
import './TinyoneFeatures.scss';

const TinyoneFeatures = () => (

	<section className="tinyone-features">
		
		<div className="container small">
			<OpeningText title={data.title} subtitle={data.subtitle} />
		</div>

		<div className="container">

			<ul className="tinyone-features__content">
				{data.content.map((item, index) =>
					<li className="tinyone-features__content__box" key={index}>
						<i className={`tinyone-features__content__box__icon ${item.icon}`}></i>
						<h4 className="tinyone-features__content__box__title">{item.title}</h4>
						<p className="tinyone-features__content__box__content">{item.content}</p>
					</li>
				)}
			</ul>

		</div>

	</section>

);

export default TinyoneFeatures;