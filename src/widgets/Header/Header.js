import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from './img/logo-tinyone.png';
import data from './Header.json';
import './Header.scss';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu: false
        }
    }

    hideMenu = () => {
		this.setState ({
			showMenu: false
		});
		document.body.style.overflow = "visible"
	}

    showMenu = () => {
		this.setState ({
			showMenu: true
		});
		document.body.style.overflow = "hidden"
	}

	render() {

		return (
            <header className={` header ${ this.state.showMenu ? 'shade' : '' } `}>
                <div className="container medium large header--flex">

                    <i className="header__button fas fa-bars" onClick={ () => { this.showMenu() } } />

                    <h1 className="header__logo">
                        <Link to={data.logo.url} className="header__logo__link">
                            <img src={logo} alt={data.logo.alt}/>
                        </Link>
                    </h1>

                    <nav className={` header__nav ${ this.state.showMenu ? 'show' : '' } `}>

                        <div className="header__nav__header">
                            <i className="header__nav__header__button fas fa-times" onClick={ () => { this.hideMenu() } } />
                        </div>

                        <ul className="header__nav__list">

                            {data.nav.map((item, index) =>

                                <li className="header__nav__list__item" key={index}>

                                    {item.type === "internal" ? (
                                        <Link to={item.url} className="header__nav__list__item__link" onClick={ () => { this.hideMenu() } }>{item.linkName}</Link>
                                    ) : (
                                        <a href={item.url} className="header__nav__list__item__link" target="_blank" rel="noopener noreferrer">{item.linkName}</a>
                                    )}

                                </li>

                            )}

                        </ul>

                    </nav>

                </div>
            </header>
		);
	}
};
