import React from 'react';
import { Link } from 'react-router-dom';
import OpeningText from '../../components/OpeningText/OpeningText';
import data from './Footer.json';
import './Footer.scss';

const Footer = () => (

	<footer className="footer">
		<div className="container small">
			
			<OpeningText title={data.title} subtitle={data.subtitle}/>

			<form className="footer__form">
				<input type="text" className="footer__form__text-field" placeholder={data.form.inputText.placeholder} required />
				<button className="footer__form__button">{data.form.button.label}</button>
			</form>

			<ul className="footer__social-medias">
				{data.socialMedias.map((item, index) =>
					<li key={index}>
						<a href={item.url}  className={`footer__social-medias__icon ${item.icon}`}></a>
					</li>
				)}
			</ul>

		</div>

		<div className="container">

			<div className="footer__extra">

				<ul className="footer__extra__contact">
					<li className="footer__extra__contact_adress">
						{data.extra.adress}
					</li>
					<li className="footer__extra__contact_email">
						<a href={`mailto:${data.extra.email}`}>{data.extra.email}</a>
					</li>
					<li className="footer__extra__contact_phone">
					<a href={`tel:${data.extra.phone}`}>{data.extra.phone}</a>
					</li>
				</ul>

				<ul className="footer__extra__links">
					{data.extra.linksGroup1.map((item, index) =>
						<li className="footer__extra__links__item" key={index}>
							<Link to={item.url} className="footer__extra__links__item__link">		{item.linkName}
							</Link>
						</li>
					)}
				</ul>

				<ul className="footer__extra__links">
					{data.extra.linksGroup2.map((item, index) =>
						<li className="footer__extra__links__item" key={index}>
							<Link to={item.url} className="footer__extra__links__item__link">		{item.linkName}
							</Link>
						</li>
					)}
				</ul>

				<ul className="footer__extra__links">
					{data.extra.linksGroup3.map((item, index) =>
						<li className="footer__extra__links__item" key={index}>
							<Link to={item.url} className="footer__extra__links__item__link">		{item.linkName}
							</Link>
						</li>
					)}
				</ul>

				<ul className="footer__extra__links">
					{data.extra.linksGroup4.map((item, index) =>
						<li className="footer__extra__links__item" key={index}>
							{item.type === "internal" ? (
								<Link to={item.url} className="footer__extra__links__item__link">
									{item.linkName}
								</Link>
							) : ( 
								<a href={item.url} className="footer__extra__links__item__link" target="_blank" rel="noopener noreferrer">
									{item.linkName}
								</a>
							)}
						</li>
					)}
				</ul>

			</div>
		</div>
		
	</footer>

);

export default Footer;