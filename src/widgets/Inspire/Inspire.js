import React, { Component } from 'react';
import Slider from "react-slick";
import OpeningText from '../../components/OpeningText/OpeningText';
import imgTabletTinyone from './img/tablet-tinyone.png';
import data from './Inspire.json';
import './Inspire.scss';

export default class Inspire extends Component {

	render () {

	const settings = {
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1
	};

		return (

			<section className="inspire">
				
				<div className="container">

					<Slider {...settings}>

						<React.Fragment>
							<div className="inspire__left-area">
								<OpeningText title={data.title} subtitle={data.subtitle} text={data.text} />
								{data.platforms.map((item, index) =>
									<i className={`inspire__left-area__icon ${item.icon}`} title={item.title} key={index} />
								)}
							</div>

							<div className="inspire__right-area">
									<img className="inspire__right-area__slide1-img" src={imgTabletTinyone} />
							</div>
						</React.Fragment>

						<div className="inspire__others-slides">
							<div className="inspire__others-slides__slide">Slide 2</div>
						</div>

						<div className="inspire__others-slides">
							<div className="inspire__others-slides__slide">Slide 3</div>
						</div>
					
					</Slider>

				</div>

			</section>

		)
	}
}