import React, { Component } from 'react';

import { BrowserRouter } from 'react-router-dom';
import Routes from './pages/Routes';

import Header from './widgets/Header/Header';
import Footer from './widgets/Footer/Footer';

export default class Global extends Component {
	render() {
		return (
			<BrowserRouter>
				<div>
					<Header />
					<Routes />
					<Footer />
				</div>
			</BrowserRouter>
		);				
	}
};
