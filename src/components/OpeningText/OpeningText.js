import React from 'react';
import './OpeningText.scss';

const OpeningText = (props) => (

	<div className="opening-text">

        { props.title && 
            <h2 className="opening-text__title">{props.title}</h2>
        }

		{ props.subtitle && 
            <h3 className="opening-text__subtitle">{props.subtitle}</h3>
        }

        { props.text && 
            <p className="opening-text__text">{props.text}</p>
        }
        
	</div>

);

export default OpeningText;