import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import Home from '../pages/Home/Home'
import Features from '../pages/Features/Features'
import Support from '../pages/Support/Support'

import Examples from '../pages/Examples/Examples';
import Shop from '../pages/Shop/Shop';
import License from '../pages/License/License';
import Contact from '../pages/Contact/Contact';
import About from '../pages/About/About';
import Privacy from '../pages/Privacy/Privacy';
import Terms from '../pages/Terms/Terms';
import Download from '../pages/Download/Download';
import Documents from '../pages/Documents/Documents';
import Media from '../pages/Media/Media';

export default props =>
    <Switch>
        <Route exact path = "/" component={Home} />
        <Route path = "/features" component={Features} />
        <Route path = "/support" component={Support} />
        
        <Route path = "/examples" component={Examples} />
        <Route path = "/shop" component={Shop} />
        <Route path = "/license" component={License} />
        <Route path = "/contact" component={Contact} />
        <Route path = "/about" component={About} />
        <Route path = "/privacy" component={Privacy} />
        <Route path = "/terms" component={Terms} />
        <Route path = "/download" component={Download} />
        <Route path = "/documents" component={Documents} />
        <Route path = "/media" component={Media} />

        <Redirect from = "*" to = "/" />
    </Switch>