import React, { Component } from 'react';
import Inspire from '../../widgets/Inspire/Inspire';
import TinyoneFeatures from '../../widgets/TinyoneFeatures/TinyoneFeatures';

export default class Home extends Component {

    render() {
        return (
            <React.Fragment>

                <Inspire />
                <TinyoneFeatures />

            </React.Fragment>
        )
    }

}