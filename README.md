# Stack de desenvolvimento

- Extração de assets: Photoshop CC
- Gerenciador de pacotes: NPM/Yarn
- IDE: VS Code (com `.editorconfig` para formatação de código)
- Lib/Framework JS: React.js (create-react-app)
- Linguagem de marcação: JSX/HTML5
- Pré-processador CSS: SASS/SCSS
- Servidor: Node.js

**CSS**

- Animações utilizando `transform: scale` focando em performance
- Animações em botões utilizando `@keyframes` com efeito gelatina
- Famílias tipográficas importadas via CDN do Google Fonts
- Ícones importados via CDN do Font Awesome
- Metodologia BEM (Block Element Modifier)
- Mobile-first com definição de breakpoints utilizando mixin
- SASS/SCSS via dependência `node-sass`

**HTML**

- Conteúdo estático importado via `.json`
- Favicon gerado a partir de PNG e incluído no projeto
- Link de e-mail com tag auxiliar `mailto:` para envio direto de e-mail
- Link de telefone com tag auxiliar `tel:` para direcionamento de chamada no celular
- Links externos sempre utilizando `target="_blank"`
- Validações de campos de formulários usando `required`
- Websemântica com foco em SEO

**Javascript**

- Comportamento de menu mobile tratado via state
- Diretórios organizados em /pages, importando /widgets
- Listas dinâmicas feitas com `.map`, importando dados de `.json`
- Uso de Pure functions em alguns componente e widgets, focando em performance
- Utilização de dependência `react-router-dom` para controle de rotas em navegação interna no Header e no Footer
- Utilização de dependência `react-slick` para implementação de slider com customizações
- Validação de `props` com condicionais short-circuit
- Validação de URL's externas ou internas com ternários

# Instruções pra rodar o projeto local

- Baixe o projeto: `git clone https://thallysbezerra@bitbucket.org/thallysbezerra/teste-front-end.git`
- Instale as dependências a partir do diretório `teste-front-end`: `npm i` ou `yarn install`
- Rode o projeto: `npm start` ou `yarn start`

# Instruções pra rodar o projeto online

- Acesse a url [https://tinyone-thallysbezerra.firebaseapp.com/](https://tinyone-thallysbezerra.firebaseapp.com/)
